import math
import itertools
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd



EMBEDDING_WEIGHTS_INIT_SCALE = 0.05

target_id_label = ["car","football"]
pub_id_label = ["car.gr","sport.gr"]
user_id_label = ["car.like","sport.like"]

CTR = np.empty([2,2,2])
CTR[0, 0, 0] = 0.95
CTR[0, 0, 1] = 0.73
CTR[0, 1, 0] = 0.73
CTR[0, 1, 1] = 0.73

CTR[1, 0, 0] = 0.73
CTR[1, 0, 1] = 0.73
CTR[1, 1, 0] = 0.73
CTR[1, 1, 1] = 0.95


def get_ctr(C, t, p, u):
    return C[t, p, u]

def gen_data(C, t, p, u, samples):
    ctr = get_ctr(C, t, p, u)
    c = np.random.random_sample((samples,))
    c[c >= 1 - ctr] = 1
    c[c < 1 - ctr] = 0
    return pd.DataFrame({'target_id': [t] * samples,
                         'pub_id': [p] * samples,
                         'user_id': [u] * samples,
                         'click': c.tolist()})

def embed_categorical(input_placeholder, labels, embed_dim, apply_dropout_placeholder=None, dropout_rate=None):

    uniform = tf.random_uniform(shape=[len(labels), embed_dim],
                                minval=-EMBEDDING_WEIGHTS_INIT_SCALE,
                                maxval=EMBEDDING_WEIGHTS_INIT_SCALE)
    embedding_look_up = tf.Variable(initial_value=uniform)

    embed = tf.nn.embedding_lookup(embedding_look_up, input_placeholder, name='lookup')
    if dropout_rate > 0.0:
        embed = tf.layers.dropout(inputs=embed, rate=dropout_rate, training=apply_dropout_placeholder)

    return embed, embedding_look_up

def create_interactions(latent_vectors):

    features = []
    for x, y in itertools.combinations(latent_vectors.keys(), r=2):
        vector_x = latent_vectors[x]
        vector_y = latent_vectors[y]
        cosine_sim = tf.reduce_sum(vector_x * vector_y, axis=-1, keep_dims=True, name='_and_'.join([x, y]))
        features.append(cosine_sim)
        print cosine_sim

    return features


def df_batch_generator(df, batch_size, n_batches, shuffle):
    if shuffle:
        indices = np.random.permutation(len(df.index))
    else:
        indices = range(0, len(df.index))

    for i in xrange(n_batches):
        batch_start_index = i * batch_size
        batch_end_index = (i + 1) * batch_size
        ind_sel = indices[batch_start_index:batch_end_index]
        yield df.iloc[ind_sel]

def plot_emb(emb, labels, color_value):
    plt.plot([0], [0], 'x', color="black")
    plt.plot(emb[:, 0], emb[:, 1], 'o', color=color_value)
    for i in range(len(emb)):
        plt.text(emb[i, 0], emb[i, 1], labels[i])
    plt.axis((-1.5, 1.5, -1.5, 1.5))


# Generate training dataset
samples=2000
frames = []
for t in range(CTR.shape[0]):
    for p in range(CTR.shape[1]):
        for u in range(CTR.shape[2]):
            frames.append(gen_data(CTR, t, p, u, samples))
train_df = pd.concat(frames, ignore_index=True)


# Model fn
embed_dim = 2
dropout_rate = 0.0  #0.3 #
batch_size = 100
learning_rate = 0.01
lr_decay_rate = 0.1
lr_decay_epochs = 1
epochs = 3*lr_decay_epochs


target_id = tf.placeholder(tf.int64, shape=[None])
pub_id = tf.placeholder(tf.int64, shape=[None])
user_id = tf.placeholder(tf.int64, shape=[None])
apply_dropout_placeholder = tf.placeholder(tf.bool)
y_placeholder = tf.placeholder(tf.float32, shape=[None, 1])

latent_vectors = {}
look_up_table = {}
latent_vectors['target'], look_up_table['target'] = embed_categorical(target_id, range(CTR.shape[0]), embed_dim,
                                                                      apply_dropout_placeholder, dropout_rate)
latent_vectors['pub'], look_up_table['pub'] = embed_categorical(pub_id, range(CTR.shape[1]), embed_dim,
                                                                apply_dropout_placeholder, dropout_rate)
latent_vectors['user'], look_up_table['user'] = embed_categorical(user_id, range(CTR.shape[2]), embed_dim,
                                                                  apply_dropout_placeholder, dropout_rate)

for key in latent_vectors.keys():
    latent_vectors[key] = tf.nn.l2_normalize(latent_vectors[key], dim=-1)
    look_up_table[key] = tf.nn.l2_normalize(look_up_table[key], dim=-1)

interactions = create_interactions(latent_vectors)

concat_layer = tf.concat(interactions, axis=-1)
logits = tf.reduce_sum(concat_layer, axis=-1, keep_dims=True)

loss = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=y_placeholder)
prediction = tf.nn.sigmoid(logits, name="prediction")


n_batches = len(train_df)/batch_size

decay_steps = n_batches * lr_decay_epochs
global_step = tf.Variable(0, name='global_step', trainable=False)
decay_learning_rate = tf.train.exponential_decay(
    learning_rate=learning_rate,  # Base learning rate.
    global_step=global_step,  # Current index into the dataset.
    decay_steps=decay_steps,
    decay_rate=lr_decay_rate,
    staircase=True)

optimizer = tf.train.AdamOptimizer(learning_rate=decay_learning_rate)
train_op = optimizer.minimize(loss, global_step=global_step)



with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    losses = []
    lrs = []
    for epoch in range(epochs):

        batch_gen = df_batch_generator(df=train_df, batch_size=batch_size, n_batches = n_batches, shuffle=True)
        for curr_batch, batch_df in enumerate(batch_gen):
            feed_dict = {target_id: batch_df['target_id'].values,
                         pub_id: batch_df['pub_id'].values,
                         user_id: batch_df['user_id'].values,
                         y_placeholder: np.reshape(batch_df['click'].values, [-1, 1]),
                         apply_dropout_placeholder: True}
            fetches = [train_op, loss, prediction, decay_learning_rate,
                       look_up_table['target'], look_up_table['pub'], look_up_table['user']]

            _, loss_val, prediction_val, lr_val, target, pub, user = sess.run(fetches, feed_dict=feed_dict)

            if (curr_batch % 5 == 0) and (epoch==0):
                plot_emb(target, target_id_label, "red")
                plot_emb(pub, pub_id_label, "blue")
                plot_emb(user, user_id_label, "black")
                plt.show(block=False)
                plt.pause(0.5)
                plt.close('all')
                #display.clear_output(wait=True)
                #display.display(plt.gcf())
                #display.display(plt.clf())

            losses.append(np.mean(loss_val))
            lrs.append(np.mean(lr_val))

    plot_emb(target, target_id_label, "red")
    plot_emb(pub, pub_id_label, "blue")
    plot_emb(user, user_id_label, "black")
    plt.show()

    #plt.figure()
    plt.plot(losses, label='loss')
    plt.title('loss')
    plt.show()


    # plt.figure()
    # plt.plot(lrs, label='lr')
    # plt.title('learning rate')
    # plt.show()