# Based on https://www.youtube.com/watch?v=aa9AC3dTKUQ

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.contrib.distributions import Bernoulli


class VariationalDenseLayer:
    """Variational Dense Layer Class"""
    def __init__(self, n_in, n_out, keep_prob, lam):
        self.keep_prob = keep_prob
        self.lam = lam
        self.bernoulli_distr = Bernoulli(probs=self.keep_prob, dtype=tf.float32)
        self.original_W = tf.Variable(tf.truncated_normal([n_in, n_out], stddev=0.01))
        self.W = tf.matmul(tf.diag(self.bernoulli_distr.sample((n_in, ))), self.original_W)
        self.beta = tf.Variable(tf.zeros([n_out]))

    def __call__(self, X, activation=tf.identity):
        output = activation(tf.matmul(X, self.W) + self.beta)
        if self.original_W.shape[1] == 1:
            output = tf.squeeze(output)
        return output

    def regularization(self):
        return self.lam * (self.keep_prob * tf.reduce_sum(tf.square(self.original_W)) + tf.reduce_sum(tf.square(self.beta)))

# Created sample data.
n_samples = 100
X_data = np.random.normal(size=(n_samples, 1))
y_data = np.random.normal(np.cos(5.*X_data) / (np.abs(X_data) + 1.), 0.1).ravel()
X_data = np.hstack((X_data, X_data**2, X_data**3))

# Points of evaluation
X_grid = np.atleast_2d(np.linspace(-3., 3., num=100)).T
X_grid = np.hstack((X_grid, X_grid**2, X_grid**3))

# Create the TensorFlow model.
n_feats = X_data.shape[1]
n_hidden = 100
keep_prob = 0.9
lam = 1e-2

X = tf.placeholder(tf.float32, [None, n_feats])
y = tf.placeholder(tf.float32, [None])
L_1 = VariationalDenseLayer(n_feats, n_hidden, keep_prob, lam)
L_2 = VariationalDenseLayer(n_hidden, n_hidden, keep_prob, lam)
L_3 = VariationalDenseLayer(n_hidden, 1, keep_prob, lam)
out_1 = L_1(X, tf.nn.relu)
out_2 = L_2(out_1, tf.nn.relu)
y_pred = L_3(out_2)

# Metrics
sse = tf.reduce_sum(tf.square(y - y_pred))
mse = sse / n_samples
loss = (
    # Negative log-likelihood.
    sse +
    # Regularization.
    L_1.regularization() +
    L_2.regularization() +
    L_3.regularization()
) / n_samples
train_step = tf.train.AdamOptimizer(1e-3).minimize(loss)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # Optimization
    for i in range(10000):
        sess.run(train_step, {X: X_data, y: y_data})
        if i % 100 == 0:
            mse_val = sess.run(mse, {X: X_data, y: y_data})
            print("Iteration {}. Mean squared error: {:.4f}.".format(i, mse_val))

    # Sample from the posterior.
    n_post = 1000
    Y_post = np.zeros((n_post, X_grid.shape[0]))
    for i in range(n_post):
        Y_post[i] = sess.run(y_pred, {X: X_grid})


for f in range(n_feats):
    plt.figure(figsize=(8, 6))
    for i in range(n_post):
        plt.plot(X_grid[:, f], Y_post[i], "b-", alpha=1. / 200)
    plt.plot(X_data[:, f], y_data, "r.")
    plt.grid()
    plt.show()

